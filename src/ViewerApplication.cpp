#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"

#include "utils/gltf.hpp"
#include "utils/images.hpp"
#include <stb_image_write.h>
#include <tiny_gltf.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

bool ViewerApplication::loadGltFile(tinygltf::Model &model)
{
  std::clog << "Loading file " << m_gltfFilePath << std::endl;

  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret =
      loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    std::cerr << warn << std::endl;
  }

  if (!err.empty()) {
    std::cerr << err << std::endl;
  }

  if (!ret) {
    std::cerr << "Failed to parse glTF file" << std::endl;
    return false;
  }

  return true;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{
  // Create a vector of GLuint with the correct size (model.buffers.size())
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
  // use glGenBuffers to create buffer objects.
  glGenBuffers(model.buffers.size(), bufferObjects.data());
  // In a loop, fill each buffer object with the data using glBindBuffer and
  // glBufferStorage. The data should be obtained from
  // model.buffers[bufferIdx].data.
  for (size_t bufferIdx = 0; bufferIdx < model.buffers.size(); bufferIdx++) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[bufferIdx].data.size(),
        model.buffers[bufferIdx].data.data(), 0);
  }
  // Don't forget to unbind the buffer object from GL_ARRAY_BUFFER after the
  // loop
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
  // This vector will contain our vertex array objects. No size given, since we
  // don't know it yet.
  std::vector<GLuint> vertexArrayObjects;

  meshIndexToVaoRange.resize(model.meshes.size());

  const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
  const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
  const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

  // Create a loop over meshes of the glTF (model.meshes).
  for (size_t meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx) {
    // Extend the size of vertexArrayObjects using resize(), by adding the
    // number of primitives of the current mesh of the loop
    // (model.meshes[meshIdx].primitives.size()).
    const auto vaoOffset = GLsizei(vertexArrayObjects.size());
    const auto &mesh = model.meshes[meshIdx];
    auto &vaoRange = meshIndexToVaoRange[meshIdx];
    vertexArrayObjects.resize(vaoOffset + mesh.primitives.size());

    vaoRange.count =
        GLsizei(mesh.primitives.size()); // One VAO for each primitive

    // Push back the corresponding range of vertex array objects in
    // meshIndexToVaoRange (see code below).
    /*    meshIndexToVaoRange.push_back(VaoRange{vaoOffset,
            GLsizei(mesh.primitives.size())}); // Will be used during
       rendering*/

    // The VAOs for are new primitives span the range [vaoOffset, vaoOffset +
    // model.meshes[meshIdx].primitives.size()]. We need to call
    // glGenVertexArrays on this range of data to create our VAOs.
    glGenVertexArrays(vaoRange.count, &vertexArrayObjects[vaoOffset]);
    //(vaoOffset + model.meshes[meshIdx].primitives.size())]));
    // Remember that &vertexArrayObjects[vaoOffset] is a pointer the the start
    // of the range, and the size of the range is
    // model.meshes[meshIdx].primitives.size(). This is important for the next

    // Create a loop over the primitives of the current mesh (so this second
    // loop is inside the first one). Inside that loop, get the VAO
    // corresponding to the primitive (vertexArrayObjects[vaoOffset +
    // primitiveIdx]) and bind it (glBindVertexArray).
    for (size_t primitiveIdx = 0; primitiveIdx < mesh.primitives.size();
         ++primitiveIdx) {
      const auto &primitive = mesh.primitives[primitiveIdx];
      const auto vao = vertexArrayObjects[vaoOffset + primitiveIdx];
      glBindVertexArray(vao);

      // Now inside that new loop we will need to enable and initialize the
      // parameters for each vertex attribute (POSITION, NORMAL, TEXCOORD_0).
      // The good news is, the code for each one is the same (we will duplicate
      // it but it can easily be factorized with a loop over ["POSITION",

      // "NORMAL", "TEXCOORD_0"]). The bad news is, we will have many
      // indirections here, so a high potential of errors. Also each attribute
      // is optional, se wo need to get use find on the std::map of attributes,
      // which returns an iterator.
      { // I'm opening a scope because I want to reuse the variable iterator in
        // the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("POSITION");
        // If "POSITION" has been found in the map
        // (*iterator).first is the key "POSITION", (*iterator).second is the
        // value, ie. the index of the accessor for this attribute
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          // get the correct tinygltf::Accessor from model.accessors
          const auto &accessor = model.accessors[accessorIdx];

          // get the correct tinygltf::BufferView from model.bufferViews.
          // You need to use the accessor
          const auto &bufferView = model.bufferViews[accessor.bufferView];

          // get the index of the buffer used by the bufferView (you need
          // to use it)
          const auto bufferIdx = bufferView.buffer;

          // get the correct buffer object from the buffer index
          const auto bufferObject = bufferObjects[bufferIdx];

          // Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray
          // (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be
          // defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);

          assert(GL_ARRAY_BUFFER == bufferView.target);
          // Theorically we could also use bufferView.target, but it is safer
          // Here it is important to know that the next call
          // Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          // Compute the total byte offset
          // using the accessor and the buffer view
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          // Call glVertexAttribPointer with the correct arguments.
          // Remember size is obtained with accessor.type, type is obtained with
          // accessor.componentType. The stride is obtained in the bufferView,
          // normalized is always GL_FALSE, and pointer is the byteOffset (don't
          // forget the cast).
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      // "NORMAL" and their corresponding VERTEX_ATTRIB_*)
      { // I'm opening a scope because I want to reuse the variable iterator in
        // the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("NORMAL");
        // If "POSITION" has been found in the map
        // (*iterator).first is the key "POSITION", (*iterator).second is the
        // value, ie. the index of the accessor for this attribute
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          // get the correct tinygltf::Accessor from model.accessors
          const auto &accessor = model.accessors[accessorIdx];

          // get the correct tinygltf::BufferView from model.bufferViews.
          // You need to use the accessor
          const auto &bufferView = model.bufferViews[accessor.bufferView];

          // get the index of the buffer used by the bufferView (you need
          // to use it)
          const auto bufferIdx = bufferView.buffer;

          // get the correct buffer object from the buffer index
          const auto bufferObject = bufferObjects[bufferIdx];

          // Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray
          // (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be
          // defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);

          assert(GL_ARRAY_BUFFER == bufferView.target);
          // Theorically we could also use bufferView.target, but it is safer
          // Here it is important to know that the next call
          // Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          // Compute the total byte offset
          // using the accessor and the buffer view
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          // Call glVertexAttribPointer with the correct arguments.
          // Remember size is obtained with accessor.type, type is obtained with
          // accessor.componentType. The stride is obtained in the bufferView,
          // normalized is always GL_FALSE, and pointer is the byteOffset (don't
          // forget the cast).
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      { // I'm opening a scope because I want to reuse the variable iterator in
        // the code for NORMAL and TEXCOORD_0
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        // If "POSITION" has been found in the map
        // (*iterator).first is the key "POSITION", (*iterator).second is the
        // value, ie. the index of the accessor for this attribute
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          // get the correct tinygltf::Accessor from model.accessors
          const auto &accessor = model.accessors[accessorIdx];

          // get the correct tinygltf::BufferView from model.bufferViews.
          // You need to use the accessor
          const auto &bufferView = model.bufferViews[accessor.bufferView];

          // get the index of the buffer used by the bufferView (you need
          // to use it)
          const auto bufferIdx = bufferView.buffer;

          // get the correct buffer object from the buffer index
          const auto bufferObject = bufferObjects[bufferIdx];

          // Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray
          // (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be
          // defined at the top of the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);

          assert(GL_ARRAY_BUFFER == bufferView.target);
          // Theorically we could also use bufferView.target, but it is safer
          // Here it is important to know that the next call
          // Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          // Compute the total byte offset
          // using the accessor and the buffer view
          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          // Call glVertexAttribPointer with the correct arguments.
          // Remember size is obtained with accessor.type, type is obtained with
          // accessor.componentType. The stride is obtained in the bufferView,
          // normalized is always GL_FALSE, and pointer is the byteOffset (don't
          // forget the cast).
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      if (primitive.indices >= 0) {
        const auto accessorIdx = primitive.indices;
        // get the correct tinygltf::Accessor from model.accessors
        const auto &accessor = model.accessors[accessorIdx];

        // get the correct tinygltf::BufferView from model.bufferViews.
        // You need to use the accessor
        const auto &bufferView = model.bufferViews[accessor.bufferView];

        // get the index of the buffer used by the bufferView (you need
        // to use it)
        const auto bufferIdx = bufferView.buffer;

        // get the correct buffer object from the buffer index
        const auto bufferObject = bufferObjects[bufferIdx];

        assert(GL_ELEMENT_ARRAY_BUFFER == bufferView.target);
        // Theorically we could also use bufferView.target, but it is safer
        // Here it is important to know that the next call
        // Bind the buffer object to GL_ARRAY_BUFFER
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject);
      }
    }
  }
  glBindVertexArray(0);
  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(
    const tinygltf::Model &model) const
{
  std::vector<GLuint> texturesObjects(model.textures.size(), 0);
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);
  // Generate the texture objects
  glGenTextures(GLsizei(model.textures.size()), texturesObjects.data());

  for (size_t modelTextureId = 0; modelTextureId < model.textures.size();
       ++modelTextureId) {
    const auto &texture = model.textures[modelTextureId];
    assert(texture.source >= 0); // ensure a source image is present
    const auto &sampler =
        texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    const auto &image = model.images[texture.source];
    // Bind texture object to target GL_TEXTURE_2D;
    glBindTexture(GL_TEXTURE_2D, texturesObjects[modelTextureId]);
    // Fill the texture object with the data from the image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }

  glBindTexture(GL_TEXTURE_2D, 0);
  return texturesObjects;
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
  const auto uLightDirectionLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto uLightIntensityLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
  // Binding textures for rendering ; we have to send/bind values before drawing
  // each object
  const auto uBaseColorTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");

  const auto uBaseColorFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  /////////////////////////////////////////////////////////////////////////////
  const auto uMetallicFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto uMetallicRoughnessTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  ///////////////////////Emissive part/////////////////////////////////////////
  const auto uEmissiveFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto uEmissiveTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  /////////////////////////////Occlusion part///////////////////////////////////
  const auto uOcclusionStrengthLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto uOcclusionTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uApplyOcclusionLocation =
      glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");

  // Build projection matrix
  /*  auto maxDistance = 500.f; // TODO use scene bounds instead to compute this
    maxDistance = maxDistance > 0.f ? maxDistance : 100.f;*/
  tinygltf::Model model;
  // TODO Loading the glTF file
  if (!loadGltFile(model)) {
    return -1;
  }
  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);
  const auto diag = bboxMax - bboxMin;
  auto maxDistance = glm::length(diag);

  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  /*  TrackballCameraController cameraController{
        m_GLFWHandle.window(), 0.5f * maxDistance};*/
  std::unique_ptr<CameraController> cameraController =
      std::make_unique<TrackballCameraController>(
          m_GLFWHandle.window(), 0.5f * maxDistance);

  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    const auto center = 0.5f * (bboxMax + bboxMin);
    const auto up = glm::vec3(0, 1, 0);
    const auto eye =
        diag.z > 0 ? center + diag : center + 2.f * glm::cross(diag, up);
    cameraController->setCamera(Camera{eye, center, up});

    /*cameraController.setCamera(
        Camera{glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0)});*/
  }

  // Init light parameters
  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(1, 1, 1);
  bool lightFromCamera = false;
  bool applyOcclusion = true;

  // Creation of texture objects
  std::vector<GLuint> textureObjects = createTextureObjects(model);

  // Create a single texture object with a variable GLuint whiteTexture to
  // reference it. Fill it with a single white RGBA pixel, and set sampling
  // parameters to GL_LINEAR,
  /// and wrapping parameters oto GL_REPEAT. This texture will be used for the
  /// base color of objects that have
  // no materials
  GLuint whiteTexture = 0;
  // Generate the texture object
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  float white[] = {1, 1, 1, 1};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGB, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  // Creation of Buffer Objects
  std::vector<GLuint> bufferObjects = createBufferObjects(model);

  // Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  std::vector<GLuint> vertexArrayObjects =
      createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  // Lambda function for material binding
  const auto bindMaterial = [&](const auto materialIndex) {
    // Material binding
    if (materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto pbrMetallicRoughness = material.pbrMetallicRoughness;
      if (uBaseColorFactorLocation >= 0) {
        glUniform4f(uBaseColorFactorLocation,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }
      if (uBaseColorTextureLocation >= 0) {
        auto textureObject = whiteTexture; // default

        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uBaseColorTextureLocation, 0);
      }
      if (uMetallicFactorLocation >= 0) {
        glUniform1f(uMetallicFactorLocation,
            (float)pbrMetallicRoughness.metallicFactor);
      }
      if (uRoughnessFactorLocation >= 0) {
        glUniform1f(uRoughnessFactorLocation,
            (float)pbrMetallicRoughness.roughnessFactor);
      }
      if (uMetallicRoughnessTextureLocation >= 0) {
        auto metallicTextureObject = whiteTexture;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &metallicTexture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture
                                 .index];
          if (metallicTexture.source >= 0) {
            metallicTextureObject = textureObjects[metallicTexture.source];
          }
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, metallicTextureObject);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }
      //////////////////////// Emissive part ///////////////////////////////////
      if (uEmissiveFactorLocation >= 0) {
        glUniform3f(uEmissiveFactorLocation, (float)material.emissiveFactor[0],
            (float)material.emissiveFactor[1],
            (float)material.emissiveFactor[2]);
      }
      if (uEmissiveTextureLocation >= 0) {
        auto emissiveTextureObject = whiteTexture;
        if (material.emissiveTexture.index >= 0) {
          const auto &emissiveTexture =
              model.textures[material.emissiveTexture.index];
          if (emissiveTexture.source >= 0) {
            emissiveTextureObject = textureObjects[emissiveTexture.source];
          }
        }
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, emissiveTextureObject);
        glUniform1i(uEmissiveTextureLocation, 2);
      }
      //////////////////////////// Occlusion part //////////////////////////////
      if (uOcclusionStrengthLocation >= 0) {
        glUniform1f(uOcclusionStrengthLocation,
            (float)material.occlusionTexture.strength);
      }
      if (uOcclusionTextureLocation >= 0) {
        auto occlusionTextureObject = whiteTexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &occlusionTexture =
              model.textures[material.occlusionTexture.index];
          if (occlusionTexture.source >= 0) {
            occlusionTextureObject = textureObjects[occlusionTexture.source];
          }
        }
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, occlusionTextureObject);
        glUniform1i(uOcclusionTextureLocation, 3);
      }
    } else {
      if (uBaseColorFactorLocation >= 0) {
        glUniform4f(uBaseColorFactorLocation, 1.f, 1.f, 1.f, 1.f);
      }
      if (uBaseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uBaseColorTextureLocation, 0);
      }
      if (uMetallicFactorLocation >= 0) {
        glUniform1f(uMetallicFactorLocation, 1.f);
      }
      if (uRoughnessFactorLocation >= 0) {
        glUniform1f(uRoughnessFactorLocation, 1.f);
      }
      if (uMetallicRoughnessTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }
      //////////////////////// Emissive part ///////////////////////////////////
      if (uEmissiveFactorLocation >= 0) {
        glUniform3f(uEmissiveFactorLocation, 0.f, 0.f, 0.f);
      }
      if (uEmissiveTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uEmissiveTextureLocation, 2);
      }
      //////////////////////////// Occlusion part //////////////////////////////
      if (uOcclusionStrengthLocation >= 0) {
        glUniform1f(uOcclusionStrengthLocation, 0.f);
      }
      if (uOcclusionTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTextureLocation, 3);
      }
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();
    if (uLightDirectionLocation >= 0) {
      /*const auto lightDirectionInViewSpace =
          glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
      glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0],
          lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);*/
      if (lightFromCamera) {
        glUniform3f(uLightDirectionLocation, 0, 0, 1);
      } else {
        const auto lightDirectionInViewSpace = glm::normalize(
            glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
        glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0],
            lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);
      }
    }

    if (uLightIntensityLocation >= 0) {
      glUniform3f(uLightIntensityLocation, lightIntensity[0], lightIntensity[1],
          lightIntensity[2]);
    }
    if (uApplyOcclusionLocation >= 0) {
      glUniform1i(uApplyOcclusionLocation, applyOcclusion);
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // get the node associated with the nodeIdx received as parameter
          tinygltf::Node &node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          // Then we need to ensure that the node has a mesh
          if (node.mesh >= 0) {
            // Compute modelViewMatrix, modelViewProjectionMatrix, normalMatrix
            glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
            glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            glm::mat4 normalMatrix =
                glm::transpose(glm::inverse(modelViewMatrix));

            // and send all of these to the shaders with glUniformMatrix4fv.
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(normalMatrix));

            // Next step is to get the mesh of the node and iterate over its
            // primitives to draw them.
            // To draw a primitive we need the VAO that we filled for it.
            // Get the mesh and the vertex array objects range of the current
            // mesh.
            const auto &mesh = model.meshes[node.mesh];
            const auto &vaoRange = meshIndexToVaoRange[node.mesh];

            // Loop over the primitives of the mesh
            for (size_t primIdx = 0; primIdx < mesh.primitives.size();
                 ++primIdx) {
              // Each primitive of index primIdx of the mesh should has its
              // corresponding VAO at vertexArrayObjects[vaoRange.begin +
              // primIdx] if vaoRange is the range of the mesh.
              const auto vao = vertexArrayObjects[vaoRange.begin + primIdx];

              // Get the current primitive.
              const auto &primitive = mesh.primitives[primIdx];

              bindMaterial(primitive.material);
              glBindVertexArray(vao);
              // Now we need to check if the primitive has indices by testing
              // if (primitive.indices >= 0). If its the case we should use
              // glDrawElements for the drawing, if not we should use
              // glDrawArrays.
              if (primitive.indices >= 0) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset =
                    accessor.byteOffset + bufferView.byteOffset;

                glDrawElements(primitive.mode, GLsizei(accessor.count),
                    accessor.componentType, (const GLvoid *)byteOffset);
              } else {
                // Primitives doesn't have indices
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                // Then call glDrawArrays, passing it the mode of the primitive,
                // 0 as second argument, and accessor.count as last argument.
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }
          // We then have one last thing to implement, after the if
          // (node.mesh >= 0) body: we need to draw children recursively.
          for (auto children : node.children) {
            drawNode(children, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // Draw all nodes
      for (auto node : model.scenes[model.defaultScene].nodes) {
        drawNode(node, glm::mat4(1));
      }
    }
  };

  // Render to image
  if (!m_OutputPath.empty()) {
    // we need to allocate an array of unsigned char to store pixels.
    // We will use 3 components (RGB image), meaning the total size of the array
    // will be `width * height * numComponents*.
    std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * 3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(),
        [&]() { drawScene(cameraController->getCamera()); });

    // We need two more things: flip the image vertically, because OpenGL does
    // not use the same convention for that than png files, and write the png
    // file with stb_image_write library which is included in the third-parties.
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());
    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
        static int cameraControllerType = 0;
        const auto cameraControllerTypeChanged =
            ImGui::RadioButton("Trackball", &cameraControllerType, 0) ||
            ImGui::RadioButton("First Person", &cameraControllerType, 1);
        if (cameraControllerTypeChanged) {
          const auto currentCamera = cameraController->getCamera();
          if (cameraControllerType == 0) {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          } else {
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController->setCamera(currentCamera);
        }
      }
      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        static float lightTheta = 0.f;
        static float lightPhi = 0.f;

        if (ImGui::SliderFloat("theta", &lightTheta, 0, glm::pi<float>()) ||
            ImGui::SliderFloat("phi", &lightPhi, 0, 2.f * glm::pi<float>())) {
          const auto sinPhi = glm::sin(lightPhi);
          const auto cosPhi = glm::cos(lightPhi);
          const auto sinTheta = glm::sin(lightTheta);
          const auto cosTheta = glm::cos(lightTheta);
          lightDirection =
              glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
        }

        static glm::vec3 lightColor(1.f, 1.f, 1.f);
        static float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("color", (float *)&lightColor) ||
            ImGui::InputFloat("intensity", &lightIntensityFactor)) {
          lightIntensity = lightColor * lightIntensityFactor;
        }
        ImGui::Checkbox("light from camera", &lightFromCamera);
        ImGui::Checkbox("apply occlusion", &applyOcclusion);
      }

      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
